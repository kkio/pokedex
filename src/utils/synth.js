import * as R from 'ramda';

const getVoices = (synth, language = 'en') => {
  const voices = synth.getVoices();
  return R.filter(voice => voice.lang.startsWith(language), voices);
};

// picks a random voice and speaks the given text
const speak = (text, pitch = 1, rate = 1) => {
  const synth = window.speechSynthesis;
  const voices = getVoices(synth);
  const voice = voices[Math.floor(Math.random() * voices.length)];

  const utterance = new SpeechSynthesisUtterance(text);
  utterance.voice = voice;
  utterance.pitch = pitch;
  utterance.rate = rate;

  synth.speak(utterance);
};

export { speak };
