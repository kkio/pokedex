const retryPromise = (promise, retries, ...args) => {
  return promise(...args)
    .then(result => {
      return result;
    })
    .catch(err => {
      if (retries > 0) {
        console.log('retrying');
        return retryPromise(promise, retries - 1, ...args);
      }
      return err;
    });
};

export { retryPromise };
