import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Button, Container, Loader, Segment, Table } from 'semantic-ui-react';
import TypeLabel from './TypeLabel';
import * as R from 'ramda';
import { speak } from '../../../utils/synth';

export default class PokeInfo extends Component {
  constructor(props) {
    super(props);
    this.state = {
      index: 0
    };
    this.decrementIndex = this.decrementIndex.bind(this);
    this.incrementIndex = this.incrementIndex.bind(this);
    this.speak = this.speak.bind(this);
  }

  decrementIndex() {
    if (this.props.flavorText) {
      const length = this.props.flavorText.length;
      // JS % operator is a remainder, not modulo TIL
      const index = (((this.state.index - 1) % length) + length) % length;
      this.setState({
        index
      });
    }
  }

  incrementIndex() {
    if (this.props.flavorText) {
      const length = this.props.flavorText.length;
      const index = (((this.state.index + 1) % length) + length) % length;
      this.setState({
        index
      });
    }
  }

  isLoading() {
    return this.props.flavorText.length === 0 || !this.props.entry;
  }

  getTypes(entry) {
    return R.compose(
      R.map(type => type.type.name),
      R.sortBy(type => type.slot)
    )(entry.types);
  }

  getBaseStats(entry) {
    const temp = R.compose(
      R.mergeAll,
      R.map(stat => {
        const temp = {};
        temp[stat.stat.name] = stat.base_stat;
        return temp;
      })
    )(entry.stats);
    return temp;
  }

  speak() {
    speak(this.props.flavorText[this.state.index]);
  }

  render() {
    return (
      <Segment>
        {this.isLoading() ? (
          <Loader active inline="centered" />
        ) : (
          [
            <TypeLabel
              key="type-label"
              types={this.getTypes(this.props.entry)}
            />,
            <Table striped>
              <Table.Body>
                {R.map(([stat, value]) => {
                  return (
                    <Table.Row>
                      <Table.Cell>{stat}</Table.Cell>
                      <Table.Cell>{value}</Table.Cell>
                    </Table.Row>
                  );
                }, R.toPairs(this.getBaseStats(this.props.entry)))}
              </Table.Body>
            </Table>,
            <p key="flavor-text">{this.props.flavorText[this.state.index]}</p>,
            <Button.Group key="buttons">
              <Button icon="left chevron" onClick={this.decrementIndex} />
              <Button icon="microphone" onClick={this.speak} />
              <Button icon="right chevron" onClick={this.incrementIndex} />
            </Button.Group>
          ]
        )}
      </Segment>
    );
  }
}

PokeInfo.propTypes = {
  entry: PropTypes.object,
  flavorText: PropTypes.arrayOf(PropTypes.string)
};
