import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Label } from 'semantic-ui-react';
import * as R from 'ramda';
import './TypeLabel.css';

export default class TypeLabel extends Component {
  render() {
    return R.map(
      type => (
        <Label key={type} className={`type-${type.toLowerCase()}`}>
          {type}
        </Label>
      ),
      this.props.types
    );
  }
}

this.propTypes = {
  types: PropTypes.arrayOf(PropTypes.string)
};
