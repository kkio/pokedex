import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Container, Grid, Visibility } from 'semantic-ui-react';
import * as R from 'ramda';
import PokePane from './Pane/PokePane';
import PokeInfo from './Info/PokeInfo';

class PokeList extends Component {
  constructor(props) {
    super(props);

    this.handleUpdate = this.handleUpdate.bind(this);
  }

  handleUpdate() {
    const { limit, offset } = this.props;
    this.props.fetchNames(limit, offset);
  }

  render() {
    const { pokemon, selected } = this.props;
    let selectedIndex = -1;
    if (selected) {
      selectedIndex = R.findIndex(R.propEq('name', selected))(pokemon);
    }
    return (
      <Container>
        <Visibility
          fireOnMount
          continuous={true}
          onBottomPassed={this.handleUpdate}
          onBottomVisible={this.handleUpdate}
        >
          <Grid>
            {R.addIndex(R.map)((entry, index) => {
              return entry
                ? [
                    <Grid.Column
                      key={entry.name}
                      mobile={16}
                      tablet={8}
                      computer={4}
                    >
                      <PokePane
                        name={entry.name}
                        entry={entry}
                        select={this.props.selectPokemon}
                        selected={selected === entry.name}
                      />
                    </Grid.Column>,
                    (index + 1) % 4 === 0 &&
                      index - selectedIndex < 4 &&
                      index - selectedIndex >= 0 && (
                        <Grid.Column
                          key={`${entry.name}-computer`}
                          width={16}
                          only={'computer'}
                        >
                          <PokeInfo
                            entry={this.props.pokemon[selectedIndex]}
                            flavorText={this.props.flavorText[selected] || []}
                          />
                        </Grid.Column>
                      ),
                    (index + 1) % 2 === 0 &&
                      index - selectedIndex < 2 &&
                      index - selectedIndex >= 0 && (
                        <Grid.Column
                          key={`${entry.name}-tablet`}
                          width={16}
                          only={'tablet'}
                        >
                          <PokeInfo
                            entry={this.props.pokemon[selectedIndex]}
                            flavorText={this.props.flavorText[selected] || []}
                          />
                        </Grid.Column>
                      ),
                    (index + 1) % 1 === 0 &&
                      index - selectedIndex < 1 &&
                      index - selectedIndex >= 0 && (
                        <Grid.Column
                          key={`${entry.name}-mobile`}
                          width={16}
                          only={'mobile'}
                        >
                          <PokeInfo
                            entry={this.props.pokemon[selectedIndex]}
                            flavorText={this.props.flavorText[selected] || []}
                          />
                        </Grid.Column>
                      )
                  ]
                : null;
            }, pokemon)}
          </Grid>
        </Visibility>
      </Container>
    );
  }
}

PokeList.propTypes = {
  limit: PropTypes.number,
  offset: PropTypes.number,
  pokemon: PropTypes.array,
  flavorText: PropTypes.object,
  selected: PropTypes.string,
  fetchNames: PropTypes.func,
  selectPokemon: PropTypes.func
};

export default PokeList;
