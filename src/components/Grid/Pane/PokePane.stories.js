import React from 'react';

import { storiesOf } from '@storybook/react';

import PokePane from './PokePane';

storiesOf('PokePane', module).add('with text', () => (
  <PokePane name={'Bulbasaur'} />
));
