import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Container, Image, Header, Loader, Segment } from 'semantic-ui-react';
import './PokePane.css';

class PokePane extends Component {
  constructor(props) {
    super(props);
    this.select = this.select.bind(this);
  }

  select() {
    if (!this.props.entry.fetching) {
      // dispatch
      this.props.select(this.props.entry.name);
    }
  }

  render() {
    const { entry, selected } = this.props;
    let classes = 'pokemon-select';
    if (selected) {
      classes += ' selected';
    }
    return (
      <Segment className={classes} onClick={this.select}>
        {entry.fetching ? (
          <Loader active inline="centered" />
        ) : (
          <Header className="name-header" as="h2">
            <Image src={entry.sprites.front_default} size="massive" circular />
            {entry.name}
          </Header>
        )}
      </Segment>
    );
  }
}

PokePane.propTypes = {
  entry: PropTypes.object,
  select: PropTypes.func,
  selected: PropTypes.bool
};

export default PokePane;
