export const GET_POKEMON_NAMES = 'GET_POKEMON_NAMES';
export const GET_POKEMON_NAMES_DONE = 'GET_POKEMON_NAMES_DONE';
export const GET_POKEMON_NAMES_FAIL = 'GET_POKEMON_NAMES_FAIL';

export const GET_POKEMON = 'GET_POKEMON';
export const GET_POKEMON_DONE = 'GET_POKEMON_DONE';
export const GET_POKEMON_FAIL = 'GET_POKEMON_FAIL';

export const SELECT_POKEMON = 'SELECT_POKEMON';

export const FLAVOR_TEXT_ADD = 'FLAVOR_TEXT_ADD';
