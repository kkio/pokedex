import React, { Component } from 'react';
import './App.css';
import PokeContainer from './containers/PokeContainer';

class App extends Component {
  render() {
    return (
      <div className="App">
        <PokeContainer />
      </div>
    );
  }
}

export default App;
