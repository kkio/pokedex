import { SELECT_POKEMON } from '../constants/actionTypes';

const initialState = {
  selected: null
};

const queryReducer = (state = initialState, action) => {
  if (action.type === SELECT_POKEMON) {
    return {
      ...state,
      selected: action.payload
    };
  }
  return state;
};

export { queryReducer };
