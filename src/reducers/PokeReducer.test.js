import React from 'react';
import { pokeReducer } from './PokeReducer';
import {
  GET_POKEMON_NAMES,
  GET_POKEMON_NAMES_FAIL,
  GET_POKEMON_NAMES_DONE,
  GET_POKEMON,
  GET_POKEMON_DONE
} from '../constants/actionTypes';

const startState = {
  fetching: false,
  count: 0,
  limit: 20,
  offset: 0,
  names: [],
  entries: {},
  flavorText: {}
};

it('creates expected initial state', () => {
  const actual = pokeReducer(undefined, { type: 'DUMMY' });
  expect(actual).toEqual(startState);
});

it(`handles ${GET_POKEMON_NAMES} action`, () => {
  const actual = pokeReducer(startState, { type: GET_POKEMON_NAMES });
  expect(actual).toEqual({ ...startState, fetching: true });
});

it(`handles ${GET_POKEMON_NAMES_DONE} action`, () => {
  const actual = pokeReducer(startState, {
    type: GET_POKEMON_NAMES_DONE,
    payload: { count: 999, results: [{ name: 'a' }, { name: 'b' }] }
  });
  expect(actual).toEqual({
    ...startState,
    fetching: false,
    count: 999,
    names: ['a', 'b']
  });
});

it(`handles ${GET_POKEMON} action`, () => {
  const actual = pokeReducer(startState, {
    type: GET_POKEMON,
    name: 'mock'
  });
  expect(actual).toEqual({
    ...startState,
    entries: {
      mock: {
        fetching: true
      }
    }
  });
});

it(`handles ${GET_POKEMON_DONE} action`, () => {
  const state = {
    ...startState,
    entries: {
      mock: {
        fetching: true
      }
    }
  };
  const actual = pokeReducer(state, {
    type: GET_POKEMON_DONE,
    payload: {
      name: 'mock',
      a: 'alpha',
      b: 'beta'
    }
  });
  expect(actual).toEqual({
    ...state,
    entries: {
      mock: {
        name: 'mock',
        a: 'alpha',
        b: 'beta',
        fetching: false
      }
    }
  });
});
