import * as R from 'ramda';
import {
  FLAVOR_TEXT_ADD,
  GET_POKEMON,
  GET_POKEMON_DONE,
  GET_POKEMON_NAMES,
  GET_POKEMON_NAMES_DONE,
  GET_POKEMON_NAMES_FAIL
} from '../constants/actionTypes';

const initialState = {
  fetching: false,
  count: 0,
  limit: 20,
  offset: 0,
  names: [],
  entries: {},
  flavorText: {}
};

const pokeReducer = (state = initialState, action) => {
  if (action.type === GET_POKEMON_NAMES) {
    return {
      ...state,
      offset: (state.offset += state.limit),
      fetching: true
    };
  } else if (action.type === GET_POKEMON_NAMES_DONE) {
    const names = R.map(item => item.name, action.payload.results);
    return {
      ...state,
      names: [...state.names, ...names],
      count: action.payload.count,
      fetching: false
    };
  } else if (action.type === GET_POKEMON_NAMES_FAIL) {
    return {
      ...state,
      fetching: false
    };
  } else if (action.type === GET_POKEMON) {
    let newEntries = { ...state.entries };
    newEntries[action.name] = { ...state.entries[action.name], fetching: true };
    return {
      ...state,
      entries: newEntries
    };
  } else if (action.type === GET_POKEMON_DONE) {
    let newEntries = { ...state.entries };
    newEntries[action.payload.name] = { ...action.payload, fetching: false };
    return {
      ...state,
      entries: newEntries
    };
  } else if (action.type === FLAVOR_TEXT_ADD) {
    let newFlavorText = { ...state.flavorText };
    newFlavorText[action.payload.name] = action.payload.flavorText;
    return {
      ...state,
      flavorText: newFlavorText
    };
  } else {
    return state;
  }
};

export { pokeReducer };
