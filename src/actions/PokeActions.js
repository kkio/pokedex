import * as pokedex from 'pokeapi-js-wrapper';
import {
  FLAVOR_TEXT_ADD,
  GET_POKEMON,
  GET_POKEMON_DONE,
  GET_POKEMON_NAMES,
  GET_POKEMON_NAMES_DONE,
  GET_POKEMON_NAMES_FAIL,
  SELECT_POKEMON
} from '../constants/actionTypes';
import * as R from 'ramda';
import { retryPromise } from '../utils/retry';

const initPokedexClient = () => {
  const options = {
    protocol: 'https',
    // hostName: 'localhost:8000',
    versionPath: '/api/v2/',
    cache: true
  };
  return new pokedex.Pokedex(options);
};

const getPokemon = name => ({ type: GET_POKEMON, name });

const select = name => ({ type: SELECT_POKEMON, payload: name });

const addFlavorText = (name, flavorText) => ({
  type: FLAVOR_TEXT_ADD,
  payload: { name, flavorText }
});

const selectPokemon = name => dispatch => {
  const client = initPokedexClient();
  dispatch(select(name));
  client
    .getPokemonSpeciesByName(name)
    .then(response => {
      const flavorText = R.uniq(
        R.map(
          entry => entry.flavor_text,
          R.filter(
            R.pathEq(['language', 'name'], 'en'),
            response.flavor_text_entries
          )
        )
      );
      dispatch(addFlavorText(name, flavorText));
    })
    .catch(err => {});
};

const fetchNames = (limit, offset) => dispatch => {
  const client = initPokedexClient();
  const interval = { limit, offset };
  dispatch({ type: GET_POKEMON_NAMES });
  client
    .getPokemonsList(interval)
    .then(response => {
      dispatch({ type: GET_POKEMON_NAMES_DONE, payload: response });
      for (const { name } of response.results) {
        dispatch(fetchPokemon(name));
      }
    })
    .catch(err => {
      dispatch({ type: GET_POKEMON_NAMES_FAIL, payload: err });
    });
};

const fetchPokemon = name => dispatch => {
  const client = initPokedexClient();
  dispatch(getPokemon(name));
  retryPromise(client.getPokemonByName, 5, name).then(response => {
    dispatch({ type: GET_POKEMON_DONE, payload: response });
  });
};

export { fetchNames, fetchPokemon, selectPokemon, getPokemon };
