import React from 'react';
import { connect } from 'react-redux';
import PokeList from '../components/Grid/PokeList';
import { fetchNames, selectPokemon } from '../actions/PokeActions';
import * as R from 'ramda';

const select = state => {
  return {
    limit: state.pokemon.limit,
    offset: state.pokemon.offset,
    pokemon: R.map(name => state.pokemon.entries[name], state.pokemon.names),
    flavorText: state.pokemon.flavorText,
    selected: state.query.selected
  };
};

const mapDispatchToProps = dispatch => {
  return {
    fetchNames: (limit, offset) => dispatch(fetchNames(limit, offset)),
    selectPokemon: name => dispatch(selectPokemon(name))
  };
};

const PokeContainer = connect(
  select,
  mapDispatchToProps
)(PokeList);

export default PokeContainer;
